
    create table agenda (
       id bigint not null auto_increment,
        data_conclusao date,
        data_inicio date,
        descricao varchar(200) not null,
        evento varchar(200) not null,
        tipo_evento varchar(255) not null,
        ativo_id bigint,
        id_complexo_minerario bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table ativo (
       id bigint not null auto_increment,
        titulo varchar(200) not null,
        primary key (id)
    ) engine=InnoDB

    create table complexo_minerario (
       id bigint not null auto_increment,
        municipio varchar(200) not null,
        nome varchar(200) not null,
        situacao varchar(255) not null,
        uf varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table complexo_minerario_equipamentos (
       complexo_minerario_id bigint not null,
        equipamentos_id bigint not null,
        primary key (complexo_minerario_id, equipamentos_id)
    ) engine=InnoDB

    create table equipamento (
       id bigint not null auto_increment,
        categoria varchar(255) not null,
        descricao varchar(200) not null,
        quantidade bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table estrutura (
       id bigint not null auto_increment,
        categoria_risco varchar(255) not null,
        dano_potencial varchar(255) not null,
        descricao varchar(200) not null,
        inserida_politica_nacional bit not null,
        link_plano_acao varchar(200) not null,
        nivel_emergencia varchar(255) not null,
        situacao varchar(255) not null,
        tipo varchar(255) not null,
        id_complexo_minerario bigint not null,
        primary key (id)
    ) engine=InnoDB

    create table perfil (
       id bigint not null auto_increment,
        nome varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table usuario (
       id bigint not null auto_increment,
        email varchar(255),
        nome varchar(255),
        senha varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table usuario_perfis (
       usuario_id bigint not null,
        perfis_id bigint not null
    ) engine=InnoDB

    alter table agenda 
       add constraint FK94qg92vvmgcganskn9llvpg92 
       foreign key (ativo_id) 
       references ativo (id)

    alter table agenda 
       add constraint FKpiveow9lcoobco20s90ky1dqg 
       foreign key (id_complexo_minerario) 
       references complexo_minerario (id)

    alter table complexo_minerario_equipamentos 
       add constraint FKf9dr79x77jctijgeulce0ank1 
       foreign key (equipamentos_id) 
       references equipamento (id)

    alter table complexo_minerario_equipamentos 
       add constraint FKredww2v13p83f5hqtd2wmm7k2 
       foreign key (complexo_minerario_id) 
       references complexo_minerario (id)

    alter table estrutura 
       add constraint FKne54vum451f974v6yssi6xjg1 
       foreign key (id_complexo_minerario) 
       references complexo_minerario (id)

    alter table usuario_perfis 
       add constraint FK7bhs80brgvo80vhme3u8m6ive 
       foreign key (perfis_id) 
       references perfil (id)

    alter table usuario_perfis 
       add constraint FKs91tgiyagbilt959wbufiphgc 
       foreign key (usuario_id) 
       references usuario (id)
