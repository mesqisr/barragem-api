-- CREATE DATABASE
--CREATE DATABASE IF NOT EXISTS barragem;

-- USUARIOS
--DROP TABLE IF EXISTS 'usuario';
INSERT INTO USUARIO(nome, email, senha) VALUES('Admin', 'admin@complexo.com', '$2a$10$suaBL8cnCaf1iKvxvYGPg.NGqOt0pRRdVr5diDxmWyajG20OHZ8FK');

-- ATIVOS
--DROP TABLE IF EXISTS 'ativo';
INSERT INTO ATIVO (TITULO) VALUES ('Caminhão Articulado de Três de Eixos 725C2');
INSERT INTO ATIVO (TITULO) VALUES ('Bomba Peristáltica');

-- EQUIPAMENTOS
--DROP TABLE IF EXISTS 'equipamento';
INSERT INTO EQUIPAMENTO(CATEGORIA, DESCRICAO, QUANTIDADE) VALUES ('BOMBAS_E_TUBULACOES', 'Bomba peristáltica', 8);
INSERT INTO EQUIPAMENTO(CATEGORIA, DESCRICAO, QUANTIDADE) VALUES ('AUTOMACAO', 'Otimizador de Floculante', 41);
INSERT INTO EQUIPAMENTO(CATEGORIA, DESCRICAO, QUANTIDADE) VALUES ('ESCAVACAO_E_CARREGAMENTO', 'Carregadeira subterrânea LH400T', 50);
INSERT INTO EQUIPAMENTO(CATEGORIA, DESCRICAO, QUANTIDADE) VALUES ('ESCAVACAO_E_CARREGAMENTO', 'Trator D4T', 4);
INSERT INTO EQUIPAMENTO(CATEGORIA, DESCRICAO, QUANTIDADE) VALUES ('ESCAVACAO_E_CARREGAMENTO', 'Trator D11', 15);
