package com.barragem.api.v1.util;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import com.barragem.api.v1.domain.enums.TipoExtensaoArquivoEnum;
import com.barragem.api.v1.domain.enums.TipoRelatorioEnum;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;


public class ReportUtils {

	    private ResourceLoader resourceLoader;

	    private static final String CLASS_PATH = "classpath:relatorios/";

	    public ReportUtils() {
	        resourceLoader = new DefaultResourceLoader();
	    }

	    public byte[] createFile(TipoRelatorioEnum tipoRelatorioEnum, Map<String, Object> params, TipoExtensaoArquivoEnum tipoExtensaoEnum) {

	        try {
	            InputStream inputStream = getInputStream(tipoRelatorioEnum);
	            JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);

	            params.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

	            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());

	            return this.exportFile(jasperPrint, tipoExtensaoEnum);

	        } catch (Exception e) {
	            System.err.println(e);
	        	return null;
	        }
	    }

	    public byte[] createFile(TipoRelatorioEnum tipoRelatorioEnum, Map<String, Object> params, Collection<?> dados, TipoExtensaoArquivoEnum tipoExtensaoEnum) {

	        try {
	            InputStream inputStream = getInputStream(tipoRelatorioEnum);

	            JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);

	            params.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

	            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(dados));

	            return this.exportFile(jasperPrint, tipoExtensaoEnum);

	        } catch (Exception e) {
	            return null;
	        }
	    }

	    public JasperReport getJasperCompileManager(TipoRelatorioEnum tipoRelatorioEnum) throws IOException, JRException {
	        return JasperCompileManager.compileReport(getInputStream(tipoRelatorioEnum));
	    }

	    public InputStream getInputStream(TipoRelatorioEnum tipoRelatorioEnum) throws IOException {
	        StringBuilder file = new StringBuilder();
	        file.append(CLASS_PATH).append(tipoRelatorioEnum.getFilePath());
	        return resourceLoader.getResource(file.toString()).getInputStream();
	    }

	    private Image getImage(InputStream inputStream, String name) throws IOException {
	        BufferedImage buffer = ImageIO.read(inputStream);
	        File outputfile = new File(name);
	        ImageIO.write(buffer, "png", outputfile);
	        Image image = ImageIO.read(new File(name));
	        outputfile.delete();
	        return image;
	    }

	    public InputStream getInputStream(String path) throws IOException {
	        StringBuilder file = new StringBuilder();
	        file.append(CLASS_PATH).append(path);
	        return resourceLoader.getResource(file.toString()).getInputStream();
	    }

	    private byte[] exportFile(JasperPrint jasperPrint, TipoExtensaoArquivoEnum tipoExtensao) throws JRException {

	        if(tipoExtensao == TipoExtensaoArquivoEnum.DOC) {
	            JRDocxExporter docExporter = new JRDocxExporter();
	            ByteArrayOutputStream docReport = new ByteArrayOutputStream();
	            docExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
	            docExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(docReport));
	            docExporter.exportReport();
	            return docReport.toByteArray();
	        }

	        return JasperExportManager.exportReportToPdf(jasperPrint);
	    }
}
