package com.barragem.api.v1.service;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.service.IBaseService;
import com.barragem.api.v1.domain.model.Estrutura;

public interface IEstruturaService extends IBaseService<Estrutura, Filter> {

}
