package com.barragem.api.v1.service;

import java.util.List;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.service.IBaseService;
import com.barragem.api.v1.domain.model.Agenda;

public interface IAgendaService extends IBaseService<Agenda, Filter> {

	List<Agenda> buscarAgendaPorComplexo(Long idComplexo);
	
}
