package com.barragem.api.v1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.respository.BaseRepository;
import com.barragem.api.core.service.BaseServiceImpl;
import com.barragem.api.v1.domain.model.Agenda;
import com.barragem.api.v1.domain.model.ComplexoMinerario;
import com.barragem.api.v1.repository.AgendaRepository;

@Service
public class AgendaServiceImpl extends BaseServiceImpl<Agenda, Filter> implements IAgendaService {

	@Autowired
	private AgendaRepository agendaRepository;
		
	@Override
	protected BaseRepository<Agenda, Long, Filter> getRepository() {
		return agendaRepository;
	}

	@Override
	public List<Agenda> buscarAgendaPorComplexo(Long idComplexo) {
		
		ComplexoMinerario complexo = new ComplexoMinerario();
		complexo.setId(idComplexo);
		
		return this.agendaRepository.findByComplexoMinerario(complexo);
	}
}
