package com.barragem.api.v1.service;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.service.IBaseService;
import com.barragem.api.v1.domain.dto.RelatorioComplexoDto;
import com.barragem.api.v1.domain.model.ComplexoMinerario;

public interface IComplexoMinerarioService extends IBaseService<ComplexoMinerario, Filter> {

	RelatorioComplexoDto gerarRelatorioComplexo(Long idComplexo);
}
