package com.barragem.api.v1.service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.respository.BaseRepository;
import com.barragem.api.core.service.BaseServiceImpl;
import com.barragem.api.v1.domain.dto.RelatorioComplexoDto;
import com.barragem.api.v1.domain.enums.TipoExtensaoArquivoEnum;
import com.barragem.api.v1.domain.enums.TipoRelatorioEnum;
import com.barragem.api.v1.domain.model.Agenda;
import com.barragem.api.v1.domain.model.ComplexoMinerario;
import com.barragem.api.v1.exception.EntidadeNaoEncontradaException;
import com.barragem.api.v1.exception.NegocioException;
import com.barragem.api.v1.repository.ComplexoMinerioRepository;
import com.barragem.api.v1.util.ReportUtils;

@Service
public class ComplexoMinerarioServiceImpl extends BaseServiceImpl<ComplexoMinerario, Filter> implements IComplexoMinerarioService {

	@Autowired
	private ComplexoMinerioRepository complexoRepository;
	
	@Autowired
	private AgendaServiceImpl agendaService;
	
	@Override
	protected BaseRepository<ComplexoMinerario, Long, Filter> getRepository() {
		return complexoRepository;
	}

	@Override
	public RelatorioComplexoDto gerarRelatorioComplexo(Long idComplexo) {

		ReportUtils reportUtils = new ReportUtils();

		Optional<ComplexoMinerario> complexo = this.complexoRepository.findById(idComplexo);
		
		if(!complexo.isPresent()) {
			
			new EntidadeNaoEncontradaException("Registro não encontrado !");
		}
		
		
        try {

            Map<String, Object> params = new HashMap<>();

            params.put("P_COMPLEXO", complexo.get());
            params.put("P_ESTRUTURAS", complexo.get().getEstruturas());
            params.put("P_ESTRUTURAS_SUBREPORT", reportUtils.getJasperCompileManager(TipoRelatorioEnum.REL_SUB_ESTRUTURAS));
            params.put("P_EQUIPAMENTOS", complexo.get().getEquipamentos());
            params.put("P_EQUIPAMENTOS_SUBREPORT", reportUtils.getJasperCompileManager(TipoRelatorioEnum.REL_SUB_EQUIPAMENTOS));

            RelatorioComplexoDto relatorioDto = new RelatorioComplexoDto();
            relatorioDto.setRelatorio(reportUtils.createFile(TipoRelatorioEnum.REL_COMPLEXO_MINERARIO, params, TipoExtensaoArquivoEnum.PDF));
            relatorioDto.setNomeArquivo(complexo.get().getNome().toUpperCase());
            
            return relatorioDto;

        } catch (Exception e) {
        	System.err.print(e);
            throw new NegocioException("Erro ao gerar declaração !" + e);
        }

	}
	
	@Override
	public void delete(Long id) {
		
		List<Agenda> agenda = this.agendaService.buscarAgendaPorComplexo(id);
		
		if(agenda.size() > 0) {
			agenda.forEach(item -> {
				this.agendaService.delete(item.getId());
			});
		}
				
		super.delete(id);
	}
	
}
