package com.barragem.api.v1.service;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.service.IBaseService;
import com.barragem.api.v1.domain.model.Equipamento;

public interface IEquipamentoService extends IBaseService<Equipamento, Filter> {

}
