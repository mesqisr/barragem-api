package com.barragem.api.v1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.respository.BaseRepository;
import com.barragem.api.core.service.BaseServiceImpl;
import com.barragem.api.v1.domain.model.Equipamento;
import com.barragem.api.v1.repository.EquipamentoRepository;

@Service
public class EquipamentoServiceImpl extends BaseServiceImpl<Equipamento, Filter> implements IEquipamentoService {

	@Autowired
	private EquipamentoRepository equipamentoRepository;
	
	@Override
	protected BaseRepository<Equipamento, Long, Filter> getRepository() {
		return equipamentoRepository;
	}
	
}
