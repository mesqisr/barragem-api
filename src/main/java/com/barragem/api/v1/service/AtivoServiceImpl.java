package com.barragem.api.v1.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.respository.BaseRepository;
import com.barragem.api.core.service.BaseServiceImpl;
import com.barragem.api.v1.domain.model.Ativo;
import com.barragem.api.v1.repository.AtivoRepository;

@Service
public class AtivoServiceImpl extends BaseServiceImpl<Ativo, Filter> implements IAtivoService {

	@Autowired
	private AtivoRepository ativoRepository;
		
	@Override
	protected BaseRepository<Ativo, Long, Filter> getRepository() {
		return ativoRepository;
	}	
	
}
