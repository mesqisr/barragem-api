package com.barragem.api.v1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.respository.BaseRepository;
import com.barragem.api.core.service.BaseServiceImpl;
import com.barragem.api.v1.domain.model.Estrutura;
import com.barragem.api.v1.repository.EstruturaRepository;

@Service
public class EstruturaServiceImpl extends BaseServiceImpl<Estrutura, Filter> implements IEstruturaService {

	@Autowired
	private EstruturaRepository estruturaRepository;
	
	@Override
	protected BaseRepository<Estrutura, Long, Filter> getRepository() {
		return estruturaRepository;
	}
}
