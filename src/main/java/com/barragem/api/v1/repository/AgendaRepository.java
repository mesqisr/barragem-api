package com.barragem.api.v1.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.respository.BaseRepository;
import com.barragem.api.v1.domain.model.Agenda;
import com.barragem.api.v1.domain.model.ComplexoMinerario;

@Repository
public interface AgendaRepository extends BaseRepository<Agenda, Long, Filter> {
	
	List<Agenda> findByComplexoMinerario(ComplexoMinerario complexoMinerario);
	
}
