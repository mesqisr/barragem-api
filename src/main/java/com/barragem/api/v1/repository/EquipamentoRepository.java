package com.barragem.api.v1.repository;

import org.springframework.stereotype.Repository;

import com.barragem.api.core.model.Filter;
import com.barragem.api.core.respository.BaseRepository;
import com.barragem.api.v1.domain.model.Equipamento;

@Repository
public interface EquipamentoRepository extends BaseRepository<Equipamento, Long, Filter> {

}
