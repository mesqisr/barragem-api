package com.barragem.api.v1.domain.dto;

import com.barragem.api.v1.domain.model.Usuario;

public class TokenDto {
	
	private String token;
	private String tipo;
	private Usuario usuarioLogado;

	public TokenDto(String token, String tipo, Usuario usuarioLogado) {
		this.token = token;
		this.tipo = tipo;
		this.usuarioLogado = usuarioLogado;
	}

	public String getToken() {
		return token;
	}

	public String getTipo() {
		return tipo;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}	
	
}
