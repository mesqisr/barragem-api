package com.barragem.api.v1.domain.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.barragem.api.core.model.BaseModel;
import com.barragem.api.v1.domain.enums.NivelEmergenciaEnum;
import com.barragem.api.v1.domain.enums.SituacaoEnum;
import com.barragem.api.v1.domain.enums.TipoEstruturaEnum;
import com.barragem.api.v1.domain.enums.TipoRiscoEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Estrutura extends BaseModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoEstruturaEnum tipo;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private SituacaoEnum situacao;
	
	@Column(length = 200, nullable = false)
	private String descricao;
	
	@Column(length = 200, nullable = false)
	private String linkPlanoAcao;
	
	@Column(name = "inserida_politica_nacional", nullable = false)
	private Boolean inseridaPoliticaNacional;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoRiscoEnum danoPotencial;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoRiscoEnum categoriaRisco;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private NivelEmergenciaEnum nivelEmergencia;
			
	@ManyToOne(optional = false)
    @JoinColumn(name = "id_complexo_minerario")
    @JsonBackReference
    private ComplexoMinerario complexoMinerario;
	
	public TipoEstruturaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoEstruturaEnum tipo) {
		this.tipo = tipo;
	}

	public SituacaoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoEnum situacao) {
		this.situacao = situacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getLinkPlanoAcao() {
		return linkPlanoAcao;
	}

	public void setLinkPlanoAcao(String linkPlanoAcao) {
		this.linkPlanoAcao = linkPlanoAcao;
	}

	public Boolean getInseridaPoliticaNacional() {
		return inseridaPoliticaNacional;
	}

	public void setInseridaPoliticaNacional(Boolean inseridaPoliticaNacional) {
		this.inseridaPoliticaNacional = inseridaPoliticaNacional;
	}

	public TipoRiscoEnum getDanoPotencial() {
		return danoPotencial;
	}

	public void setDanoPotencial(TipoRiscoEnum danoPotencial) {
		this.danoPotencial = danoPotencial;
	}

	public TipoRiscoEnum getCategoriaRisco() {
		return categoriaRisco;
	}

	public void setCategoriaRisco(TipoRiscoEnum categoriaRisco) {
		this.categoriaRisco = categoriaRisco;
	}

	public NivelEmergenciaEnum getNivelEmergencia() {
		return nivelEmergencia;
	}

	public void setNivelEmergencia(NivelEmergenciaEnum nivelEmergencia) {
		this.nivelEmergencia = nivelEmergencia;
	}
	

	public ComplexoMinerario getComplexoMinerario() {
		return complexoMinerario;
	}

	public void setComplexoMinerario(ComplexoMinerario complexoMinerario) {
		this.complexoMinerario = complexoMinerario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estrutura other = (Estrutura) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
