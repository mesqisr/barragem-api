package com.barragem.api.v1.domain.enums;

public enum SituacaoEnum {


    EM_OPERACAO( "Em Operação"),
    EM_CONSTRUCAO( "Em Construção"),
	DESATIVADA("Desativada");
    
	private String descricao;

    SituacaoEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
