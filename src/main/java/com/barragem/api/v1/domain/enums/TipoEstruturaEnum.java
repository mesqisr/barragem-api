package com.barragem.api.v1.domain.enums;

public enum TipoEstruturaEnum {


    BARRAGEM( "Barragem"),
    MINA( "Mina"),
    ESTRTURA_ADMINISTRATIVA( "Estrutura Administrativa"),
    ESTRTURA_APOIO("Estrutura de Apoio");
    
	private String descricao;

    TipoEstruturaEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
