package com.barragem.api.v1.domain.enums;

public enum CategoriaEnum {
	
	AUTOMACAO( "Automação"),
	BOMBAS_E_TUBULACOES("Bombas e Tubulações"),
	ESCAVACAO_E_CARREGAMENTO( "Escavação e Carregamento"),
    EQUIPAMENTO_ELETRICO( "Equipamento Elétrico"),
	BRITAGEM_E_TRANSPORTADOR_POR_CORREIA("Britagem e Transportador por Correia");

    private String descricao;
    	
    CategoriaEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
