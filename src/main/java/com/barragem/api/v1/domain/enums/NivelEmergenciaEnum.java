package com.barragem.api.v1.domain.enums;

import java.io.Serializable;

public enum NivelEmergenciaEnum implements Serializable {


    NIVEL_1( "Nível 1"),
    NIVEL_2( "Nível 2"),
    NIVEL_3( "Nível 3"),
	SEM_EMERGENCIA("Sem Emergência");
    
	private String descricao;

    NivelEmergenciaEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
