package com.barragem.api.v1.domain.dto;

public class RelatorioComplexoDto {

    private byte[] relatorio;
    private String nomeArquivo;
	
    public byte[] getRelatorio() {
		return relatorio;
	}
	
    public void setRelatorio(byte[] relatorio) {
		this.relatorio = relatorio;
	}
	
    public String getNomeArquivo() {
		return nomeArquivo;
	}
	
    public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	
    
}
