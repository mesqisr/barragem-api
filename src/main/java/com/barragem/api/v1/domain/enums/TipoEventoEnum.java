package com.barragem.api.v1.domain.enums;

public enum TipoEventoEnum {

    MANUTENCAO( "Manutenção"),
    REPOSICAO( "Reposição");
    
	private String descricao;

    TipoEventoEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
