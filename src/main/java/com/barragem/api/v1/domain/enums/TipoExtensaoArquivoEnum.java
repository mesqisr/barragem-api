package com.barragem.api.v1.domain.enums;


public enum TipoExtensaoArquivoEnum {

    PDF("pdf"),
    DOC("doc");

    private String filePath;

    TipoExtensaoArquivoEnum(String filePath) {
        this.filePath = filePath;
    }

}
