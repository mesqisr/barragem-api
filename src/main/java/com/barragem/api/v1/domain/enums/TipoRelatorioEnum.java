package com.barragem.api.v1.domain.enums;

public enum TipoRelatorioEnum {

    REL_COMPLEXO_MINERARIO("complexo/complexo_minerario.jrxml"),
	REL_SUB_ESTRUTURAS("complexo/sub_estruturas.jrxml"),
	REL_SUB_EQUIPAMENTOS("complexo/sub_equipamentos.jrxml");
    
    private String filePath;

    TipoRelatorioEnum(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }
}
