package com.barragem.api.v1.domain.enums;

public enum TipoRiscoEnum {


    ALTO( "Alto"),
    MEDIO( "Médio"),
    BAIXO( "Baixo");
    
	private String descricao;

    TipoRiscoEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
