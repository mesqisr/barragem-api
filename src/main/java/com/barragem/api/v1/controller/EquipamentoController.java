package com.barragem.api.v1.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.barragem.api.core.controller.AbstractRestController;
import com.barragem.api.core.model.Filter;
import com.barragem.api.v1.domain.model.Equipamento;
import com.barragem.api.v1.service.EquipamentoServiceImpl;

@RestController
@RequestMapping("/v1/equipamentos")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EquipamentoController extends AbstractRestController<Equipamento, EquipamentoServiceImpl, Filter> {

}

