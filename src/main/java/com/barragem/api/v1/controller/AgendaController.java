package com.barragem.api.v1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.barragem.api.core.controller.AbstractRestController;
import com.barragem.api.core.model.Filter;
import com.barragem.api.v1.domain.model.Agenda;
import com.barragem.api.v1.service.AgendaServiceImpl;

@RestController
@RequestMapping("/v1/agendas")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AgendaController extends AbstractRestController<Agenda, AgendaServiceImpl, Filter> {

	@Autowired
	private AgendaServiceImpl agendaService;
			
	@GetMapping("/complexo")
	@ResponseStatus(value = HttpStatus.OK)
	public List<Agenda> findByComplexo(@RequestParam(value = "id") Long idComplexo) {
		return this.agendaService.buscarAgendaPorComplexo(idComplexo);
	} 
}

