package com.barragem.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.barragem.api.core.controller.AbstractRestController;
import com.barragem.api.core.model.Filter;
import com.barragem.api.v1.domain.dto.RelatorioComplexoDto;
import com.barragem.api.v1.domain.model.ComplexoMinerario;
import com.barragem.api.v1.service.ComplexoMinerarioServiceImpl;

@RestController
@RequestMapping("/v1/complexosMinerarios")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ComplexoMinerioController extends AbstractRestController<ComplexoMinerario, ComplexoMinerarioServiceImpl, Filter> {
	
	@Autowired
	private ComplexoMinerarioServiceImpl service;
	
	@GetMapping("/relatorio/{id}")
	public ResponseEntity<RelatorioComplexoDto> gerarRelatorio(@PathVariable Long id) {

		RelatorioComplexoDto dto = service.gerarRelatorioComplexo(id);

		if (dto.getRelatorio() != null) {
			return ResponseEntity.ok(dto);
		}

		return ResponseEntity.notFound().build();
	}

}
