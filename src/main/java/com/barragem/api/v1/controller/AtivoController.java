package com.barragem.api.v1.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.barragem.api.core.controller.AbstractRestController;
import com.barragem.api.core.model.Filter;
import com.barragem.api.v1.domain.model.Ativo;
import com.barragem.api.v1.service.AtivoServiceImpl;

@RestController
@RequestMapping("/v1/ativos")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AtivoController extends AbstractRestController<Ativo, AtivoServiceImpl, Filter> {

	
	
}

