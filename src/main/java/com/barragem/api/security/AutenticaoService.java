package com.barragem.api.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.barragem.api.v1.domain.model.Usuario;
import com.barragem.api.v1.repository.UsuarioRepository;

@Service
public class AutenticaoService implements UserDetailsService {

	@Autowired
	UsuarioRepository repository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<Usuario> usuario = repository.findByEmail(email);
		if(usuario.isPresent()) {
			return usuario.get();
		}
				
		throw new UsernameNotFoundException("Dados inválidos!");
	}

	
	
}
