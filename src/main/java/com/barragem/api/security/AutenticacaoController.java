package com.barragem.api.security;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.barragem.api.v1.domain.dto.TokenDto;
import com.barragem.api.v1.domain.model.Usuario;
import com.barragem.api.v1.repository.UsuarioRepository;
import com.barragem.api.v1.domain.dto.LoginDto;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/auth")
public class AutenticacaoController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@PostMapping
	public ResponseEntity<TokenDto> autenticar(@RequestBody @Valid LoginDto login) {
		
		UsernamePasswordAuthenticationToken dadosLogin = login.converter();
		Authentication authentication = authenticationManager.authenticate(dadosLogin);
		String token = tokenService.gerarToken(authentication);
		
	    Optional<Usuario> usuario = usuarioRepository.findByEmail(login.getEmail());

	    if (!usuario.isPresent()) {
		    throw new BadCredentialsException("usuário ou senha invalidos");
	    }

	    usuario.get().setSenha(null);
		
				
		return ResponseEntity.ok(new TokenDto(token, "Bearer", usuario.get()));
				
	}
	
}
