package com.barragem.api.core.respository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.barragem.api.core.model.BaseModel;

@NoRepositoryBean
public interface BaseRepository<T extends BaseModel, ID extends Serializable, F> extends JpaRepository<T, ID> {
		
}
