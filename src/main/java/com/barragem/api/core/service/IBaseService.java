package com.barragem.api.core.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * BaseService interface com os metodos base para ser implementados.
 * 
 * @author Danilo Pereira
 * @param T - entidade que será persistida no banco.
 * @param F - Classe de filtro que será usada para pesquisa e paginação.
 */
public interface IBaseService<T, F> {
	
	List<T> findAll();
	
	Page<T> pages(Pageable pageable);
	
	Page<T> filtrar(F filter, Pageable pageable);

	T create(T t);

	T update(T t, Long id);

	Optional<T> findById(Long id);

	void delete(Long id);	

}
