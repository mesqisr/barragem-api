package com.barragem.api.core.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.barragem.api.core.model.BaseModel;
import com.barragem.api.core.respository.BaseRepository;
import com.barragem.api.v1.exception.EntidadeNaoEncontradaException;

@Service
public abstract class BaseServiceImpl<T extends BaseModel, F> implements IBaseService<T, F> {

	protected abstract BaseRepository<T, Long, F> getRepository();

	@Override
	public List<T> findAll() {
		return getRepository().findAll();
	}

	@Override
	public T create(T t) {
		return getRepository().save(t);
	}

	@Override
	public T update(T t, Long id) {
		return getRepository().save(t);
	}

	@Override
	public Optional<T> findById(Long id) {
		
		Optional<T> entity = getRepository().findById(id);
		
		if(!entity.isPresent()) {
				
			new EntidadeNaoEncontradaException("Registro não encontrado !");
		}
				
		
		return entity;
	}

	@Override
	public void delete(Long id) {

		Optional<T> t = getRepository().findById(id);

		if (t.isPresent()) {
			getRepository().delete(t.get());
		}

	}

	@Override
	public Page<T> pages(Pageable pageable) {
		PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());
		return getRepository().findAll(pageRequest);	
	}

	@Override
	public Page<T> filtrar(F filter, Pageable pageable) {		
		return null;
	}	
}
