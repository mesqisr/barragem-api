package com.barragem.api.configuration.swagger;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

//  @Override
//  public void addCorsMappings(CorsRegistry registry) {
//      registry.addMapping("/**").allowCredentials(true).allowedOrigins("*")
//              .exposedHeaders("Access-Control-Expose-Headers", "authorization", "Cache-Control", "Content-Type", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Origin")
//              .allowedMethods("GET", "OPTIONS", "POST", "PUT", "DELETE", "PATCH");
//  }
	
	 @Override
	    public void addCorsMappings(CorsRegistry registry) {
	        registry.addMapping("/**").allowCredentials(true).allowedOrigins("*")
	                .exposedHeaders("Access-Control-Expose-Headers", "authorization", "Cache-Control", "Content-Type", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Origin")
	                .allowedMethods("GET", "OPTIONS", "POST", "PUT", "DELETE", "PATCH");
	    }
}