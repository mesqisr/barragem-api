package com.barragem.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BarragemApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BarragemApiApplication.class, args);
	}

}
